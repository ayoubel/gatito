# Commands

## Requirements

   * pip install flask
   * pip install bokeh
   * apt-get install redis **or** conda install -c anaconda redis=3.2.0
   * apt-get install nodejs **or** conda install -c conda-forge nodejs=6.10.2
   * pip install 'celery[redis]'
   * pip install httpie
   * pip install flask-cors
   * apt-get install nodejs-legacy

## Run back-end

   * redis-server
   * celery -A worker worker --loglevel=debug
   * python3 restapi.py

## Test back-end

   * http PUT http://localhost:5000 f='sqrt(4 - xs**2)' a:=0 b:=2 c:=0 d:=2
   * http GET http://localhost:5000/1

## Build front-end

   * cd front-end
   * npm install
   * npm install --global gulp
   * node_modules/.bin/gulp watch

# Architecture

## Architecure Source

   * https://youtu.be/eEXKIp8h0T0
   * http://bit.ly/dutc-tutorial
   * gatito/tutorial

## General Architecture : Technologies

1. Back-end
   * *DataBase ? : SQLite*
   * Algorithm/function : python
   * REST API : flask (JSON API layer), celery (job management system)

2. Front-End
   * Simple single-page app : React + Redux Babel (for JSX transpilation) and Gulp as a build system.
   * Interaction : Bokeh
   * mobile apps using the same infrastructure ? : React Native
   * Server : python script, EngineX?

```mermaid
graph LR
   subgraph Back-End
      DataBase-->Engine
      Algorithm-->Engine
      Engine-->WebService
   end

   subgraph Front-End
      WebService-.->|API|Explorer
      Explorer-->Bokeh
      Explorer-->Interface
   end
```

# TOPIC

# Goal

We wish to create an interactive web page which is able to perform some simple data analysis.
Our goal is to create a very simple example of how to put into production an algorithm.
The complexity of the algorithm is therefore not the main topic within this porject.


# Data sources

   * https://toolbox.google.com/datasetsearch
   * https://www.data.gouv.fr/fr/
   * https://opendata.paris.fr/page/home/

# Authors

AEL, EKA, LZO
